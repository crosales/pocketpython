format ELF
extrn printf
public main
section '.data'
format_string db "%d ", 0
c dd 5

section '.text'
main:
push ebp
mov ebp, esp
sub esp, 20

mov eax, 2
mov dword[ebp - 4], eax

mov eax, 1
mov dword[ebp - 8], eax
xor ebx, ebx
mov eax, dword[ebp - 4]
cmp eax, 2
setg bl 
mov dword[ebp - 12], ebx 

cmp dword[ebp - 12], 0
je label_0

push dword[ebp - 4]
push format_string
call printf
add esp, 8

jmp label_1
label_0:

push dword[ebp - 8]
push format_string
call printf
add esp, 8
xor ebx, ebx
mov eax, dword[ebp - 8]
cmp eax, 3
setg bl 
mov dword[ebp - 16], ebx 

label_2:
cmp dword[ebp - 16], 0
je label_3
xor ebx, ebx
mov eax, dword[ebp - 8]
cmp eax, 1
sete bl 
mov dword[ebp - 20], ebx 

cmp dword[ebp - 20], 0
je label_4

mov eax, 5
mov dword[ebp - 8], eax

jmp label_5
label_4:
label_5:

jmp label_2
label_3:

label_1:
leave
ret



